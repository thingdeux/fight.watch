# Fight.Watch #

This is the code repo. for the website [Fight.Watch](http://fight.watch), a web portal using twitch API for easy access to fight game streams.

### Technology Used ###

* Python 2.7
* CherryPy
* SQLAlchemy (and mySQL)
* Jinja2
* Twitch.API